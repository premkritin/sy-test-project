resource "aws_lb_target_group" "sy-target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "sy-app-tg"
  port        = 8080
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

resource "aws_lb" "sy-aws-alb" {
  name     = "sy-app-alb"
  internal = false

  security_groups = [aws_security_group.sy-asg-sg.id]

  subnets = [var.subnet1, var.subnet2, var.subnet3]

  tags = {
    Name = "sy-app-alb"
  }

  ip_address_type    = "ipv4"
  load_balancer_type = "application"
}

resource "aws_lb_listener" "sy-test-alb-listner" {
  load_balancer_arn = aws_lb.sy-aws-alb.arn
  port              = 443
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.sy-target-group.arn
  }
}