
resource "aws_launch_configuration" "sy-test-launch-config" {
  image_id        = var.ami
  instance_type   = var.instance_type
  security_groups = [aws_security_group.sy-asg-sg.id]
  key_name = "ubuntu1_key"
  spot_price    = "0.03"
  enable_monitoring = true
  

 

  # provisioner "local-exec" {
  #   command = "echo ${"aws_launch_configuration.sy-test-launch-config.public_dns"} > inventory"
  # }

  # provisioner "local-exec" {
  #   command = "ansible all -m shell -a 'apt -y install nginx; systemctl restart nginx'"
  # }


  # depends_on = [
  #   aws_launch_configuration.sy-test-launch-config
  # ]
}

   




resource "aws_security_group" "sy-asg-sg" {
  name   = "sy-asg-sg"
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "inbound_ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.sy-asg-sg.id
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_http" {
  from_port         = 8080
  protocol          = "tcp"
  security_group_id = aws_security_group.sy-asg-sg.id
  to_port           = 8080
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_http_80" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.sy-asg-sg.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_https" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.sy-asg-sg.id
  to_port           = 443
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.sy-asg-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_autoscaling_group" "sy-test" {
  launch_configuration = aws_launch_configuration.sy-test-launch-config.name
  vpc_zone_identifier  = [var.subnet1, var.subnet2 , var.subnet3]
  target_group_arns    = [aws_lb_target_group.sy-target-group.arn]
  health_check_type    = "EC2"

  min_size = 2
  max_size = 4

  tag {
    key                 = "Name"
    value               = "sy-app-asg"
    propagate_at_launch = true
  }
}

resource "aws_autoscalingplans_scaling_plan" "sy-test" {
  name = "sy-test-dynamic-cost-optimization"

  application_source {
    tag_filter {
      key    = "application"
      values = ["sy-test"]
    }
  }

  scaling_instruction {
    max_capacity       = 3
    min_capacity       = 0
    resource_id        = format("autoScalingGroup/%s", aws_autoscaling_group.sy-test.name)
    scalable_dimension = "autoscaling:autoScalingGroup:DesiredCapacity"
    service_namespace  = "autoscaling"

    target_tracking_configuration {
      predefined_scaling_metric_specification {
        predefined_scaling_metric_type = "ASGAverageCPUUtilization"
      }

      target_value = 70
    }
  }
}