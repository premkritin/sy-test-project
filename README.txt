# sy-test-project

############ Terraform ############

To run this example you need to execute:

$ terraform init
$ terraform plan -var-file=terraform.tfvars
$ terraform apply -var-file=terraform.tfvars

After apply we will get two ec2 server with spot instance, LB along with target group which is pointing 8080 port.
LB will redirect 443 hit to 8080
Auto Scaling created with min 2 max 4 nodes. when CPU hit 70% CPU usage it will trigger to build new ec2
EC2 server has been added to cloud watch for monitoring.

##### Ansible ##############

Ansible role has been created for installing nginx same we can use for deployment if we change something in file /roles/nginix/file/index.yml

maintainability:
ec2 maintenance I have created a log rotate role for sample purpose 
Ansible patch role has been created for the frequently OS upgrade.

